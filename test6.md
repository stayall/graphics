# 期末考查

## 准备

- 请仔细阅读以下说明，按要求完成期末考查任务
- 期末考查按项目方式，用任何C语言（不限于VC++6.0）编写源代码。
- 提交的成果包括：
  - C语言源代码以及编译好的exe可执行程序
  - 项目文档报告（格式参见：课程考查报告模板.docx）
- 提交目录：test6。所有源代码和文档放在test6子目录。test1至test5是团队的平时实验提交目录。
- 源代码可以借鉴任何资源，但不能全部抄袭。
- 项目以团队形式提交，下面有两个项目，二选一，一个团队只提交一个项目。要test6/目录中的README.md文件中写出项目名称，团队成员，成员分工。
- 团队成员应合作完成，要分摊工作量。提交后通过git提交记录体现团队成员的工作量。
- 最后提交时间为2023-1-10

## 评分方式

|评分项|评分标准|满分|得分
|---|---|---|---|
|源代码|代码正确，注释清楚|30|
|算法分析|算法分析正确|30|
|运行截图|截图符合任务要求|10|
|实验体会|体会深刻，切中主题|30|

## 项目1：二维图形几何变换及操控

- 项目名称：二维图形几何变换及操控。
- 项目要求：编写一个C语言程序，模仿的Word界面，程序在屏幕上绘制一个可封闭多边形，并进行填充。绘制过程中生成一个包围多边形，并能用鼠标/键盘进行缩放，旋转等变换。参见Word中的相关功能
- 参见第5章案例11（二维图形几何变换算法）。
- 模仿的Word界面如下，蓝色区域是多边形。周围的方框是包围多边形：
  ![test6_1.png](./img/test6_1.png)

## 项目2：Utah茶壶显示

- 项目名称：Utah茶壶显示
- 项目要求：本项目要求用C语言编写显示Utah茶壶的三维形体的源代码。
- 至少要求显示为线框图，并投影到2维空间显示，同时能进行旋转变换。
- 进一步要求是真实感显示，即用任意选择的颜色模型（如Gouraud,Phone模型），也可以加入纹理。
- 可以采用三角形面片方式，例如sample/html_sample目录中的数据和程序：
  - http://122.9.52.158:9090/
  - ![QT高级样例](./img/html_sample.png)
  - 本样例的数据文件是data2
    - verts数组中的数据表示x,y,z坐标值，是归一化以后的[0,1]区间，实际显示的时候要乘以适当的倍数。
    - tris数组中的数据是面三角形的三个点的点顺序，1代表verts的第1个点，注意C语言的数组下标是从0开始的，所以在使用的时候要减1
- 也可以采用四边形面片方式，例如sample/vc_sample/UtahCup中的数据和程序。
  - ![QT高级样例](./sample/vc_sample/UtahCup/Utah.png)
  - 本样例的数据和程序都在Utah.cpp中。以四边形面片表示。数据要比三角形面片简洁，但需要进行双二次Bezier转换。
