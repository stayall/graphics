
// TestView.cpp : CTestView 类的实现
//

#include "stdafx.h"
// SHARED_HANDLERS 可以在实现预览、缩略图和搜索筛选器句柄的
// ATL 项目中进行定义，并允许与该项目共享文档代码。
#ifndef SHARED_HANDLERS
#include "Test.h"
#endif

#include "TestDoc.h"
#include "TestView.h"
#include "math.h"//包含数学头文件
#define  PI 3.1415926//PI的宏定义
#define Round(d) int(floor(d+0.5))//四舍五入宏定义
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CTestView

IMPLEMENT_DYNCREATE(CTestView, CView)

BEGIN_MESSAGE_MAP(CTestView, CView)
	// 标准打印命令
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
	ON_COMMAND(IDM_DRAWPIC, &CTestView::OnDrawpic)
END_MESSAGE_MAP()

// CTestView 构造/析构

CTestView::CTestView()
{
	// TODO: 在此处添加构造代码
	pDC=NULL;
}

CTestView::~CTestView()
{
}

BOOL CTestView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 在此处通过修改
	//  CREATESTRUCT cs 来修改窗口类或样式

	return CView::PreCreateWindow(cs);
}

// CTestView 绘制

void CTestView::OnDraw(CDC* /*pDC*/)
{
	CTestDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: 在此处为本机数据添加绘制代码
}


// CTestView 打印

BOOL CTestView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// 默认准备
	return DoPreparePrinting(pInfo);
}

void CTestView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 添加额外的打印前进行的初始化过程
}

void CTestView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 添加打印后进行的清理过程
}


// CTestView 诊断

#ifdef _DEBUG
void CTestView::AssertValid() const
{
	CView::AssertValid();
}

void CTestView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CTestDoc* CTestView::GetDocument() const // 非调试版本是内联的
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CTestDoc)));
	return (CTestDoc*)m_pDocument;
}
#endif //_DEBUG


// CTestView 消息处理程序

void CTestView::Peano_Hilbert(int n,int s,CP2 p0,CP2 p1)//Peano-Hilbert函数
{
	double w,h;	
	if(0==n)
  	{
		CP2 p2,p3,p4,p5;
		w=p1.x-p0.x;h=p1.y-p0.y;
		p2=CP2(p0.x+w/4,p0.y+h/4);
		p3=CP2(p0.x+(2-s)*w/4,p0.y+(2+s)*h/4);
		p4=CP2(p0.x+3*w/4,p0.y+3*h/4);
		p5=CP2(p0.x+(2+s)*w/4,p0.y+(2-s)*h/4);
		pDC->MoveTo(Round(P0.x),Round(P0.y));
		pDC->LineTo(Round(p2.x),Round(p2.y));
		pDC->LineTo(Round(p3.x),Round(p3.y));
		pDC->LineTo(Round(p4.x),Round(p4.y));
		pDC->LineTo(Round(p5.x),Round(p5.y));
		P0=p5;
		return;
     }
	 CP2 p2,p3,p4,p5,p6;
	 p2=CP2((p0.x+p1.x)/2,p0.y);
	 p3=CP2(p1.x,(p0.y+p1.y)/2);
	 p4=CP2((p0.x+p1.x)/2,p1.y);
	 p5=CP2(p0.x,(p0.y+p1.y)/2);
	 p6=(p0+p1)/2;
     if(s>0)
     {
		 Peano_Hilbert(n-1,-1,p0,p6);//左
		 Peano_Hilbert(n-1,1,p5,p4);//下
         Peano_Hilbert(n-1,1,p6,p1);//下
         Peano_Hilbert(n-1,-1,p3,p2);//左
	 }
     else
     {
		 Peano_Hilbert(n-1,1,p0,p6);//下
         Peano_Hilbert(n-1,-1,p2,p3);//左
         Peano_Hilbert(n-1,-1,p6,p1);//左
         Peano_Hilbert(n-1,1,p4,p5);//下
     }
}

void CTestView::OnDrawpic() 
{
	// TODO: Add your command handler code here
	CInputDlg dlg;
	if (IDOK==dlg.DoModal())
	{
		n=dlg.m_m;
		s=dlg.m_s;
	}
	else
		return;
	RedrawWindow();	
	pDC=GetDC();
	CRect rect;//客户区矩形对象
	GetClientRect(&rect);	
	pDC->SetMapMode(MM_ANISOTROPIC);
	pDC->SetWindowExt(rect.Width(),rect.Height());
	pDC->SetViewportExt(rect.Width(),-rect.Height());
	pDC->SetViewportOrg(rect.Width()/2,rect.Height()/2);
	P0=CP2(-rect.Width()/2.0,-rect.Height()/2.0);
	P1=CP2(rect.Width()/2.0,rect.Height()/2.0);
	Peano_Hilbert(n,s,P0,P1);
	ReleaseDC(pDC);
}
