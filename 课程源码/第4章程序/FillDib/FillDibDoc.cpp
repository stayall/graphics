// FillDibDoc.cpp : implementation of the CFillDibDoc class
//

#include "stdafx.h"
#include "FillDib.h"

#include "FillDibDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
#include "filldibview.h"
/////////////////////////////////////////////////////////////////////////////
// CFillDibDoc

IMPLEMENT_DYNCREATE(CFillDibDoc, CDocument)

BEGIN_MESSAGE_MAP(CFillDibDoc, CDocument)
	//{{AFX_MSG_MAP(CFillDibDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFillDibDoc construction/destruction

CFillDibDoc::CFillDibDoc()
{
}

CFillDibDoc::~CFillDibDoc()
{
}

BOOL CFillDibDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CFillDibDoc serialization

void CFillDibDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CFillDibDoc diagnostics

#ifdef _DEBUG
void CFillDibDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CFillDibDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CFillDibDoc commands

BOOL CFillDibDoc::OnOpenDocument(LPCTSTR lpszPathName) 
{
//	if (!CDocument::OnOpenDocument(lpszPathName))
//		return FALSE;
	CString str=lpszPathName;
	if(m_Bmp.Load(str)==FALSE)
	{
		str="不能打开文件:";
		str+=lpszPathName;
		AfxMessageBox(str);
	}
	CFillDibView *pView;
	POSITION pos=GetFirstViewPosition();
	while(pos)
	{
		pView=(CFillDibView*)GetNextView(pos);
		pView->Invalidate(FALSE);
	}
	return TRUE;	
	return TRUE;
}

BOOL CFillDibDoc::OnSaveDocument(LPCTSTR lpszPathName) 
{
	// TODO: Add your specialized code here and/or call the base class
	CString str;
	if(m_Bmp.Save(lpszPathName)==FALSE)
	{
		str="不能保存文件:";
		str+=lpszPathName;
		AfxMessageBox(str);	return CDocument::OnSaveDocument(lpszPathName);
	}
	return TRUE;
}
