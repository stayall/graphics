// Image.h: interface for the CImage class.
//
//////////////////////////////////////////////////////////////////////
#include "stdafx.h"

#if !defined(AFX_IMAGE_H__F82CCA77_E041_11D7_9F52_0004764652A5__INCLUDED_)
#define AFX_IMAGE_H__F82CCA77_E041_11D7_9F52_0004764652A5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <windowsx.h> //GlobalAllocPtr()
#include <stack>
using namespace std;


#define BYTES4(bits) (((bits) + 31) / 32 * 4) 
#define IS_WIN30_DIB(lpbi)  ((*(LPDWORD)(lpbi)) == sizeof(BITMAPINFOHEADER)) 
#define DIB_HEADER_MARKER	((WORD) ('M' << 8) | 'B') 

//模板
typedef stack<LPBYTE> POSITION_STACK;//位图位序列指针堆栈
typedef stack<BYTE>  BITOFFSET_STACK;//单色，16色位偏移量堆栈

UINT GetBytesRow(BITMAPINFOHEADER *pbih);
int GetNumColors(BITMAPINFOHEADER *pbih);

class CImage : public CObject  
{
public:
	LPBYTE			m_pBits;//位序列
	LPBITMAPINFO	m_pBmpInfo;//位图头信息
private:
	HPALETTE	m_hPalette;
	HBITMAP		m_hBmp;
public:
	COLORREF GetColorTable(int index);
	void SetColorTable(int index,COLORREF color);
	int GetNumColors()
	{
		return ::GetNumColors((LPBITMAPINFOHEADER)m_pBmpInfo);
	}
	int GetHeight();
	int GetWidth();
	LPBITMAPINFO GetBitMapInfo();
	int GetNearColorNum(COLORREF color);
	BOOL Fill(POINT pnt,COLORREF color);
	void Draw(HDC hdc, int x, int y);
	HBITMAP DIBToDIBSection(LPBITMAPINFO pbi,LPBYTE pBits);
	BOOL Load(LPCSTR strFileName);
	BOOL Save(LPCSTR strFileName);
	CImage();
	virtual ~CImage();

private:
	BOOL SaveFrom(LPCSTR strFileName,LPBITMAPINFO pbih,LPBYTE pBits);
	inline void PopPixel(POSITION_STACK &ps,BITOFFSET_STACK &bs,LPBYTE &p, BYTE &BitsOffset);
	inline void PushPixel(POSITION_STACK &ps,BITOFFSET_STACK &bs,LPBYTE &p, BYTE &BitsOffset);
	void UpPixel(LPBYTE &p,int BytesRow);
	void DownPixel(LPBYTE &p,int BytesRow);
	inline void RightPixel(LPBYTE &p,BYTE &BitsOffset);
	inline void LeftPixel(LPBYTE &p,BYTE &BitsOffset);
	inline void SetColor(LPBYTE &p,BYTE &BitsOffset,long Color);
	inline long GetColor(LPBYTE &p,BYTE &BitsOffset);
	BOOL IsRequireDither(HDC hdc);
	HPALETTE CreateDIBPalette(LPBITMAPINFO pbi);
	BOOL UpdateInternal();
};

#endif // !defined(AFX_IMAGE_H__F82CCA77_E041_11D7_9F52_0004764652A5__INCLUDED_)
