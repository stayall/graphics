// InfoDlg.cpp : implementation file
//

#include "stdafx.h"

#include "FillDib.h"
#include "InfoDlg.h"
#include "FillDibView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CInfoDlg dialog


CInfoDlg::CInfoDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CInfoDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CInfoDlg)
	m_strInfo = _T("");
	//}}AFX_DATA_INIT
	m_pImage=NULL;
	m_posStart.x=10;
	m_posStart.y=60;
	m_szColor.cx=20;
	m_szColor.cy=20;
}


void CInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CInfoDlg)
	DDX_Text(pDX, IDC_INFO, m_strInfo);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CInfoDlg, CDialog)
	//{{AFX_MSG_MAP(CInfoDlg)
	ON_WM_PAINT()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CInfoDlg message handlers

BOOL CInfoDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	CString str;
	m_strInfo="";
	int ncolors;
	if(m_pImage->m_pBmpInfo)
	{
		str.Format("文件尺寸:\t%d,%d\n",m_pImage->GetWidth(),m_pImage->GetHeight());
		m_strInfo+=str;
		ncolors=m_pImage->GetNumColors();
		if(ncolors)//如果有颜色表
			str.Format("文件颜色:\t%d色\n",ncolors);
		else
			str="文件颜色:\t真彩色\n";
		m_strInfo+=str;
		if(ncolors)//如果有颜色表
			str="颜 色 表:\t见下表\n";
		else
			str="颜 色 表:\t无\n";
		m_strInfo+=str;
	}
	UpdateData(FALSE);
	m_noldsel=-1;
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CInfoDlg::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	if(m_pImage->m_pBmpInfo==NULL) return;
	int row,col;
	int i,ncolors=m_pImage->GetNumColors();
	CBrush brush[256],*oldbrush=NULL;
	if(ncolors==0) return;
	RGBQUAD *rq=m_pImage->m_pBmpInfo->bmiColors;
	CRect box;
	box.SetRect(m_posStart.x,m_posStart.y,
		m_posStart.x+m_szColor.cx+1,m_posStart.y+m_szColor.cy+1);
	//m_szColor.cx+1和m_szColor.cy+1是考虑到矩形区域的左闭右开，上闭下开。
	for(i=0,row=0;row<16 && i<ncolors;row++)
	{
		for(col=0;col<16 && i<ncolors;col++,i++)
		{
			brush[i].CreateSolidBrush(RGB(rq[i].rgbRed,rq[i].rgbGreen,rq[i].rgbBlue));
			if(oldbrush==NULL)
				oldbrush=dc.SelectObject(&brush[i]);			
			else
				dc.SelectObject(&brush[i]);
			dc.Rectangle(&box);
			box.OffsetRect(m_szColor.cx,0);	
		}
		box.OffsetRect(-16*m_szColor.cx,m_szColor.cy);
	}
	dc.SelectObject(oldbrush);
}

void CInfoDlg::OnMouseMove(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	if(m_pImage->m_pBmpInfo==NULL) return;
	int row,col;
	int ncolors=m_pImage->GetNumColors();
	if(ncolors==0) return;
	RGBQUAD *rq=m_pImage->m_pBmpInfo->bmiColors;
	CRect box;
	box.SetRect(m_posStart.x,m_posStart.y,
		m_posStart.x+m_szColor.cx,m_posStart.y+m_szColor.cy);
	point.x-=m_posStart.x;
	point.y-=m_posStart.y;

	CRect rect;
	CClientDC dc(this);
	row=point.y/m_szColor.cy;
	col=point.x/m_szColor.cx;
	if(row<16 && col<16)
		m_nselcolor=row*16+col;
	else
	{
		m_nselcolor=-1;
		return;	
	}
	if(m_nselcolor>=0 && m_nselcolor<ncolors && m_nselcolor!=m_noldsel)
	{	
		if(m_noldsel>=0)
		{
			rect.SetRect(m_noldsel%16*m_szColor.cx,m_noldsel/16*m_szColor.cy,
				(m_noldsel%16+1)*m_szColor.cx,m_noldsel/16*m_szColor.cy+m_szColor.cy);
			rect.OffsetRect(m_posStart);
			dc.DrawFocusRect(&rect);
		}
		rect.SetRect(col*m_szColor.cx,row*m_szColor.cy,
			col*m_szColor.cx+m_szColor.cx,row*m_szColor.cy+m_szColor.cy);
		rect.OffsetRect(m_posStart);
		dc.DrawFocusRect(&rect);
		m_noldsel=m_nselcolor;
	}
	CDialog::OnMouseMove(nFlags, point);
}

void CInfoDlg::OnLButtonDown(UINT nFlags, CPoint point) 
{
	int ncolors=m_pImage->GetNumColors();
	if(ncolors==0) return;
	if(m_pImage->m_pBmpInfo==NULL) return;
	if(m_nselcolor>=0 && m_nselcolor<ncolors)
	{
		CColorDialog dlg(m_pImage->GetColorTable(m_nselcolor));
		if(dlg.DoModal()==IDOK)
		{
			m_pImage->SetColorTable(m_nselcolor,dlg.GetColor());
			Invalidate(FALSE);
			m_noldsel=-1;
		}	
	}
	CDialog::OnLButtonDown(nFlags, point);
}
