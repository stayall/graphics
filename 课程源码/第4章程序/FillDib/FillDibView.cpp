// FillDibView.cpp : implementation of the CFillDibView class
//

#include "stdafx.h"
#include "FillDib.h"

#include "FillDibDoc.h"
#include "FillDibView.h"
#include "mainfrm.h"
#include "infodlg.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFillDibView

IMPLEMENT_DYNCREATE(CFillDibView, CView)

BEGIN_MESSAGE_MAP(CFillDibView, CView)
	//{{AFX_MSG_MAP(CFillDibView)
	ON_WM_LBUTTONDOWN()
	ON_COMMAND(IDD_SHOWINFO, OnShowinfo)
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFillDibView construction/destruction

CFillDibView::CFillDibView()
{

}

CFillDibView::~CFillDibView()
{
}

BOOL CFillDibView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CFillDibView drawing

void CFillDibView::OnDraw(CDC* pDC)
{
	CFillDibDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	pDoc->m_Bmp.Draw(pDC->m_hDC,0,0);
	// TODO: add draw code for native data here
}

/////////////////////////////////////////////////////////////////////////////
// CFillDibView printing

BOOL CFillDibView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CFillDibView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CFillDibView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CFillDibView diagnostics

#ifdef _DEBUG
void CFillDibView::AssertValid() const
{
	CView::AssertValid();
}

void CFillDibView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CFillDibDoc* CFillDibView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CFillDibDoc)));
	return (CFillDibDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CFillDibView message handlers
void CFillDibView::OnLButtonDown(UINT nFlags, CPoint point) 
{
	CFillDibDoc *pDoc=GetDocument();	
	pDoc->m_Bmp.Fill(point,((CMainFrame*)AfxGetMainWnd())->m_FillColor);
	Invalidate(FALSE);	
	CView::OnLButtonDown(nFlags, point);
}

void CFillDibView::OnShowinfo() 
{
	CFillDibDoc *pDoc=GetDocument();	
	CInfoDlg dlg(this);
	dlg.m_pImage=&pDoc->m_Bmp;
	if(dlg.DoModal()==IDOK)
	{
		Invalidate(FALSE);
	}
}
