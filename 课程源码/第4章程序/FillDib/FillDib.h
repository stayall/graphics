// FillDib.h : main header file for the FILLDIB application
//

#if !defined(AFX_FILLDIB_H__1CFE6AE5_E3AA_11D7_9F52_0004764652A5__INCLUDED_)
#define AFX_FILLDIB_H__1CFE6AE5_E3AA_11D7_9F52_0004764652A5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CFillDibApp:
// See FillDib.cpp for the implementation of this class
//

class CFillDibApp : public CWinApp
{
public:
	CFillDibApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFillDibApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CFillDibApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FILLDIB_H__1CFE6AE5_E3AA_11D7_9F52_0004764652A5__INCLUDED_)
