// InputDialog3.cpp : implementation file
//

#include "stdafx.h"
#include "testCallDialog.h"
#include "InputDialog3.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CInputDialog3 dialog


CInputDialog3::CInputDialog3(CWnd* pParent /*=NULL*/)
	: CDialog(CInputDialog3::IDD, pParent)
{
	//{{AFX_DATA_INIT(CInputDialog3)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CInputDialog3::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CInputDialog3)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CInputDialog3, CDialog)
	//{{AFX_MSG_MAP(CInputDialog3)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CInputDialog3 message handlers
